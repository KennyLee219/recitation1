/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Kenny Lee
 */
public class Recitation1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        Button btn2 = new Button();
        btn2.setText("Say 'Goodbye Cruel World'");
        btn2.setOnAction(e -> {
           System.out.println("Goodbye Cruel World!");
            }
        );
        StackPane root = new StackPane();
       /* btn.setLayoutX(100);
        btn.setLayoutY(100);
        
        btn2.setLayoutX(200);
        btn2.setLayoutY(200);
        Only work for other types of panes */
       btn.setTranslateX(-50);
        btn.setTranslateY(-50);
        
        btn2.setTranslateX(50);
        btn2.setTranslateY(100);
      root.getChildren().addAll(btn, btn2);
       
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
